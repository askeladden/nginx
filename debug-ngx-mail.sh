#! /bin/bash

DIR=test-ngx-mail
test -x ./objs/nginx -a -f $DIR/logs/nginx.pid && ./objs/nginx -p $DIR -s stop

pkill -f "tail -n0 -f $DIR/logs/error.log" >/dev/null 2>&1

rm -f $DIR/conf/nginx.conf

test -e Makefile || ./configure --with-mail --with-debug --without-http_rewrite_module || exit 1
make -j20 >/dev/null || exit 1

mkdir -p $DIR/{conf,html,logs,run}
touch $DIR/html/auth

test -e $DIR/conf/nginx.conf || cat > $DIR/conf/nginx.conf <<EOF
worker_processes 1;
error_log logs/error.log debug;

events {
  worker_connections 1024;
}

http {
  server {
    listen 127.0.0.2:14301;
    location /auth {
      add_header Auth-Status OK;
#      add_header Auth-Server 91.207.158.17;
#      add_header Auth-Port 143;
      add_header Auth-Server 127.0.0.3;
      add_header Auth-Port 14302;
    }
  }
}

mail {
  proxy on;
  imap_capabilities "IMAP4rev1" "UIDPLUS";
  auth_http 127.0.0.2:14301/auth;

  server {
    listen     127.0.0.2:14300;
    protocol   imap;
  }
}
EOF

./objs/nginx -p $DIR
tail -n0 -f $DIR/logs/error.log &
trap 'kill $(jobs -p)' EXIT SIGINT SIGTERM SIGQUIT
(
echo -ne 'PROXY TCP4 255.255.255.255 255.255.255.255 65535 65535\r\n'
sleep 0.5
echo -ne 'a1 login username p@ssw0rd\r\n'
)| nc 127.0.0.2 14300

